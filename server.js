const express = require("express");
const app = express();
const router = require("./routes");
const bodyparser = require("body-parser");
const cors = require("cors");
app.use(cors());
app.use(bodyparser.json());
app.use("/", router);
app.listen(9000, () => {
  console.log(`server running at 9000`);
});
