const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();
const getpratice = async (req, res) => {
  try {
    const pratice = await prisma.pratice.findMany();
    res.send(pratice);
  } catch (error) {
    console.log(error);
    res.send(error);
  }
};
const addpratice = async (req, res) => {
  try {
    const pratice = await prisma.pratice.create({ data: req.body });
    res.send(pratice);
  } catch (error) {
    console.log(error);
    res.send(error);
  }
};
module.exports.pratice = { getpratice, addpratice };
