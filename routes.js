const express = require("express");
const router = express.Router();
const { pratice } = require("./api/pratice")
router.get("/pratice/get", pratice.getpratice);
router.post("/pratice/add", pratice.addpratice);
module.exports = router;